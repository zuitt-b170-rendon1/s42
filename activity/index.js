const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

const updateFullName = () => {
	let firstName = txtFirstName.value
	let lastName = txtLastName.value

	spanFullName.innerHTML = `${firstName} ${lastName}`;
}

txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName);


/*txtFirstName.addEventListener("keyup", (event) => {
	if (txtFullName.innerHTML !== txtFirstName & txtFullName.innerHTML!== txtLastName) 
	{
		return
	}

})


txtFirstName.addEventListener("keyup", (event2) => {
	txtFullName.innerHTML = txtFirstName.value;
})

txtLastName.addEventListener("keyup", (event2) => {
	txtFullName.innerHTML = txtLastName.value;
})*/





